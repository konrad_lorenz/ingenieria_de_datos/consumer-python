import requests
import os
import concurrent.futures
import time

def fetch_and_write_data(endpoint):
    api_url = f"https://jsonplaceholder.typicode.com/todos/{endpoint}"

    response = requests.get(api_url)
    if response.status_code != 200:
        print(f"Error al realizar la solicitud HTTP para {endpoint}: {response.status_code}")
        return
    print(response.status_code, response.json())

    filename = f"data_{endpoint}.json"
    with open(filename, "wb") as file:
        file.write(response.content)

def main():
    startTime = time.time()

    with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
        for i in range(1, 201):
            endpoint = str(i)
            executor.submit(fetch_and_write_data, endpoint)

    totalTime = time.time() - startTime
    print(f"Tiempo total de ejecución: {totalTime} segundos")

if __name__ == "__main__":
    main()
